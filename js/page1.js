function italicParagraphs(id) {
    var p = document.getElementById(id);
    p.style.fontStyle = "italic";
}
italicParagraphs("part1");

function colorTableRows(id) {
    document.getElementById(id).style.backgroundColor = "red";
}
// colorTableRows('mytable');


function makeBootstrapTable(id) {
    var mytable = document.getElementById(id);

    for (var line = 0; line < mytable.rows.length; line++) {
        var myrow = mytable.rows[line];
        myrow.style.border = "1px solid green";
        for (var column = 0; column < myrow.cells.length; column++) {
            var mycell = myrow.cells[column];
            mycell.style.border = "1px solid balck";
            mycell.style.backgroundColor = 'rgba(' + 0 + "," + line * 225 + "," + column * 225 + ',0.1)';

        }
    }
}
makeBootstrapTable('mytable')


function makeButtonTextRed(id) {
    var button = document.getElementById(id);
    button.style.color = 'red';
}
makeButtonTextRed('btn01');
makeButtonTextRed('btn04');

function makeButtonSmallRed(id) {
    var button = document.getElementById(id);
    button.classList.remove('btn', 'btn-success', 'btn-sm');
    button.classList.add('btn-small', 'btn-danger');
}
makeButtonSmallRed('btn02');

function removeButton(id) {
    var button = document.getElementById(id);
    button.parentNode.removeChild(button);
}
removeButton('btn08');



function changeImagesInPart3() {
    var image = document.getElementsByTagName('img');
    console.log(image.length);
    image[0].className += 'rounded-circle';
    image[1].className += 'rounded';
    image[2].className += 'img-thumbnail';
}
changeImagesInPart3();

function makeLiBorderInUl(id) {
    var list = document.getElementById(id);
    var items = list.getElementsByTagName('li');
    for (var i = 0; i < items.length; i++) {
        items[i].style.border = "2px solid red";
    }
}
makeLiBorderInUl('ulist');

function addLiInOlist(id, max_number) {
    var list = document.getElementById(id);
    for (i = 1; i <= max_number; i++) {
        var myli = document.createElement('LI');
        myli.innerHTML += "New Fruit " + i;
        list.appendChild(myli);
    }
}
addLiInOlist('olist', 20);